package fun.huixi.weiju.pojo.dto.appeal;

import fun.huixi.weiju.page.PageQuery;
import lombok.Data;

import java.math.BigDecimal;

/**
 *  分页查询诉求
 * @Author 叶秋
 * @Date 2021/11/15 22:05
 * @param
 * @return
 **/
@Data
public class PageQueryAppealDTO extends PageQuery {

    // 分页参数已经继承


    /**
     * 经度
     **/
    private BigDecimal longitude;

    /**
     * 纬度
     **/
    private BigDecimal latitude;

}
