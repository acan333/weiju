package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.exception.BusinessException;
import fun.huixi.weiju.pojo.entity.WjAppealEndorse;
import fun.huixi.weiju.mapper.WjAppealEndorseMapper;
import fun.huixi.weiju.service.WjAppealEndorseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 此表为记录诉求点赞用途，用来判断此人是否已经点赞 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjAppealEndorseServiceImpl extends ServiceImpl<WjAppealEndorseMapper, WjAppealEndorse> implements WjAppealEndorseService {

    /**
     * 查询用户是否点赞了该动态
     *
     * @param userId       用户id
     * @param appealIdList 诉求id
     * @return 诉求id <-> 是否点赞
     * @Author 叶秋
     * @Date 2021/11/15 22:30
     **/
    @Override
    public Map<Integer, Boolean> queryUserOrEndorseByUserId(Integer userId, List<Integer> appealIdList) {

        List<WjAppealEndorse> list = this.lambdaQuery()
                .in(WjAppealEndorse::getAppealId, appealIdList)
                .eq(WjAppealEndorse::getUserId, userId)
                .list();

        // 这个用户点赞过的动态id
        List<Integer> endorseAppealIdList =
                list.stream().map(WjAppealEndorse::getAppealId).collect(Collectors.toList());

        HashMap<Integer, Boolean> userIdToEndorseMap = new HashMap<>(appealIdList.size());

        appealIdList.stream().forEach(item -> {

            if(endorseAppealIdList.contains(item)){
                userIdToEndorseMap.put(item, true);
            }else{
                userIdToEndorseMap.put(item, false);
            }

        });

        return userIdToEndorseMap;
    }

    /**
     * 判断用户是否点赞了该诉求
     *
     * @param userId   用户id
     * @param appealId 诉求id
     * @return java.lang.Boolean
     * @Author 叶秋
     * @Date 2021/11/16 10:48
     **/
    @Override
    public Boolean judgeUserOrEndorse(Integer userId, Integer appealId) {

        Integer count = this.lambdaQuery()
                .eq(WjAppealEndorse::getUserId, userId)
                .eq(WjAppealEndorse::getAppealId, appealId)
                .count();

        if(count > 0){
            return true;
        }

        return false;
    }

    /**
     * 点赞该诉求
     *
     * @param userId   用户id
     * @param appealId 诉求id
     * @return void
     * @Author 叶秋
     * @Date 2021/11/16 10:51
     **/
    @Override
    public void endorseAppeal(Integer userId, Integer appealId) {

        WjAppealEndorse wjAppealEndorse = new WjAppealEndorse();
        wjAppealEndorse.setAppealId(appealId);
        wjAppealEndorse.setUserId(userId);

        boolean save = this.save(wjAppealEndorse);

        if(!save){
            throw new BusinessException("点赞失败");
        }


    }

    /**
     * 取消点赞该诉求
     *
     * @param userId   用户id
     * @param appealId 诉求id
     * @return void
     * @Author 叶秋
     * @Date 2021/11/16 10:51
     **/
    @Override
    public void cancelEndorseAppeal(Integer userId, Integer appealId) {

        List<WjAppealEndorse> list = this.lambdaQuery()
                .eq(WjAppealEndorse::getUserId, userId)
                .eq(WjAppealEndorse::getAppealId, appealId)
                .list();
        List<Integer> collect = list.stream()
                .map(WjAppealEndorse::getAppealEndorseId).collect(Collectors.toList());

        boolean b = this.removeByIds(collect);

        if(!b){
            throw new BusinessException("取消点赞失败");
        }

    }
}
