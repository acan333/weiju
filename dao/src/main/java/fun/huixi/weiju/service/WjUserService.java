package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjUser;
import com.baomidou.mybatisplus.extension.service.IService;
import fun.huixi.weiju.pojo.vo.user.UserTokenVO;

import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjUserService extends IService<WjUser> {

    /**
     *  查询用户信息 用于token加密处理
     * @Author 叶秋
     * @Date 2021/9/14 10:47
     * @param userId 普通用户id
     * @return com.xinzhengcheng.mingji.pojo.vo.user.UserTokenVO
     **/
    UserTokenVO queryUserTokenInfo(Integer userId);



    /**
     *  查询用户名是否存在
     * @Author 叶秋
     * @Date 2021/11/2 23:33
     * @param nickName 用户昵称
     * @return fun.huixi.weiju.pojo.entity.WjUser
     **/
    WjUser loadUserByNickName(String nickName);



    /**
     *  判断用户名称是否存在
     * @Author 叶秋
     * @Date 2021/11/10 23:36
     * @param nickName
     * @return boolean
     **/
    boolean judgeNickNameExist(String nickName);


    /**
     *  判断该邮箱是否存在
     * @Author 叶秋
     * @Date 2021/11/10 23:38
     * @param email
     * @return boolean
     **/
    boolean judgeEmailExist(String email);



    /**
     *  根据用户id集合查询用户的基本信息
     * @Author 叶秋
     * @Date 2021/11/15 22:22
     * @param userIds 用户id集合
     * @return 用户id <-> 用户基本信息
     **/
    Map<Integer, WjUser> queryUserInfoByUserIds(Set<Integer> userIds);
}
