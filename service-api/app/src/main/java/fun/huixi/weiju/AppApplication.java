package fun.huixi.weiju;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *  应用启动类
 * @Author 叶秋 
 * @Date 2021/11/2 22:57
 * @param 
 * @return 
 **/
@EnableAsync
@EnableScheduling
@EnableTransactionManagement
@SpringBootApplication
@MapperScan(value = "fun.huixi.weiju.mapper")
public class AppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);
    }

}
