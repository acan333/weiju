package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjAppealAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 存储诉求的地址信息 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjAppealAddressMapper extends BaseMapper<WjAppealAddress> {

}
