package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 诉求与标志的对应关系
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-10
 */
@Data
@Accessors(chain = true)
@TableName("wj_appeal_tag_center")
public class WjAppealTagCenter {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 诉求id
     */
    @TableField("appeal_id")
    private Integer appealId;

    /**
     * 标签id
     */
    @TableField("tag_id")
    private Integer tagId;


}
