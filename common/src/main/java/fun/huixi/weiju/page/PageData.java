package fun.huixi.weiju.page;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页查询最终传过去的值
 * @Author 叶秋
 * @Date 2020/6/2 21:56
 * @param
 * @return
 **/
@Data
public class PageData<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *  数据
     **/
    private List<T> records;

    /**
     *  总条数
     **/
    private Integer total;

    /**
     *  每页大小
     **/
    private Integer size;

    /**
     *  页数
     **/
    private Integer current;


}