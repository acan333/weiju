package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjAppealMaterial;
import fun.huixi.weiju.mapper.WjAppealMaterialMapper;
import fun.huixi.weiju.service.WjAppealMaterialService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjAppealMaterialServiceImpl extends ServiceImpl<WjAppealMaterialMapper, WjAppealMaterial> implements WjAppealMaterialService {

}
