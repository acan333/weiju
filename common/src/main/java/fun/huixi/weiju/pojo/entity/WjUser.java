package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.util.Collection;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_user")
public class WjUser extends BaseEntity implements UserDetails {

    private static final long serialVersionUID=1L;

    /**
     * 用户id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 头像对应的URL地址
     */
    @TableField("head_portrait")
    private String headPortrait;

    /**
     * 性别 改用String，活泼一点。自定义都可以
     */
    @TableField("sex")
    private String sex;

    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 电子邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 个性签名（冗余）
     */
    @TableField("signature")
    private String signature;

    /**
     * 简介
     */
    @TableField("introduce")
    private String introduce;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * 密码（冗余）
     */
    @TableField("password")
    private String password;











    /**
     * 用户角色
     */
    @TableField(exist = false)
    private Collection<GrantedAuthority> authorities;
    /**
     * 账户是否过期
     */
    @TableField(exist = false)
    private boolean isAccountNonExpired = false;
    /**
     * 账户是否被锁定
     */
    @TableField(exist = false)
    private boolean isAccountNonLocked = false;
    /**
     * 证书是否过期
     */
    @TableField(exist = false)
    private boolean isCredentialsNonExpired = false;
    /**
     * 账户是否有效
     */
    @TableField(exist = false)
    private boolean isEnabled = true;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }


    @Override
    public String getUsername() {
        return this.nickName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }



}
