package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 聊天室对应的用户
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_chat_user")
public class WjChatUser extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "chat_user_id", type = IdType.AUTO)
    private Integer chatUserId;

    /**
     * 聊天室id
     */
    @TableField("chat_id")
    private Integer chatId;

    /**
     * 聊天室对应的用户id（一对一，一对多）
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 退出聊天的时间（一对多的聊天室可以退出群聊，需要个退出时间）
     */
    @TableField("quit_time")
    private LocalDateTime quitTime;



}
