package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjAppealMaterial;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjAppealMaterialService extends IService<WjAppealMaterial> {

}
