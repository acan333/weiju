package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjAppeal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 诉求表 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjAppealService extends IService<WjAppeal> {

}
